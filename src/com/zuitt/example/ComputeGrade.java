package com.zuitt.example;

import java.util.Scanner;

public class ComputeGrade {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter firstname:");
        String firstName = myObj.nextLine();
        System.out.println("Enter lastname:");
        String lastName = myObj.nextLine();

        //compute grade
        System.out.println("First Subject Grade:");
        float grd1 = myObj.nextFloat();
        System.out.println("Second Subject Grade:");
        float grd2 = myObj.nextFloat();
        System.out.println("Third Subject Grade:");
        float grd3 = myObj.nextFloat();

        int averageGrade = Math.round(((grd1+grd2+grd3) / 3) * 1);


        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your average grade is: " + averageGrade);


    }
}
